include(CMakeFindDependencyMacro)
find_dependency(glm)
find_dependency(glbinding)

include(${CMAKE_CURRENT_LIST_DIR}/globjects-export.cmake)
