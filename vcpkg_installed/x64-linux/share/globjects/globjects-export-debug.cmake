#----------------------------------------------------------------
# Generated CMake target import file for configuration "Debug".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "globjects::globjects" for configuration "Debug"
set_property(TARGET globjects::globjects APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(globjects::globjects PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/debug/lib/libglobjectsd.a"
  )

list(APPEND _cmake_import_check_targets globjects::globjects )
list(APPEND _cmake_import_check_files_for_globjects::globjects "${_IMPORT_PREFIX}/debug/lib/libglobjectsd.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
