#----------------------------------------------------------------
# Generated CMake target import file for configuration "Debug".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "tinyfiledialogs::tinyfiledialogs" for configuration "Debug"
set_property(TARGET tinyfiledialogs::tinyfiledialogs APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(tinyfiledialogs::tinyfiledialogs PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "C"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/debug/lib/libtinyfiledialogs.a"
  )

list(APPEND _cmake_import_check_targets tinyfiledialogs::tinyfiledialogs )
list(APPEND _cmake_import_check_files_for_tinyfiledialogs::tinyfiledialogs "${_IMPORT_PREFIX}/debug/lib/libtinyfiledialogs.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
