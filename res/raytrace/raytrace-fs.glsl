#version 400
#extension GL_ARB_shading_language_include : require
#include "/raytrace-globals.glsl"

struct Sphere {
    vec3 origin;
    float radius;
    vec3 color;
    float reflectivity;
    float specularFalloff;
};

struct Box {
    vec3 minimumBounds;
    vec3 maximumBounds;
    vec3 color;
    float reflectivity;
    float specularFalloff;
};

struct Plane {
    vec3 center;
    vec3 normal;
    vec3 color;
    float reflectivity;
    float specularFalloff;
};

struct Light {
    vec3 position;
    vec3 color;
    float intensity;
};

uniform mat4 modelViewProjectionMatrix;
uniform mat4 inverseModelViewProjectionMatrix;

uniform vec3 ambientColor;
uniform float ambientIntensity;

uniform float attenConstant;
uniform float attenLinear;
uniform float attenQuadratic;

uniform int reflectionCount;
uniform float reflectionDecayRatio;

in vec2 fragPosition;
out vec4 fragColor;

uniform int sphereCount;
uniform Sphere spheres[10];

uniform int boxCount;
uniform Box boxes[10];

uniform int planeCount;
uniform Plane planes[10];

uniform int lightCount;
uniform Light lights[10];


float calcDepth(vec3 pos)
{
	float far = gl_DepthRange.far; 
	float near = gl_DepthRange.near;
	vec4 clip_space_pos = modelViewProjectionMatrix * vec4(pos, 1.0);
	float ndc_depth = clip_space_pos.z / clip_space_pos.w;
	return (((far - near) * ndc_depth) + near + far) / 2.0;
}


// Sphere is hit if the square root in the quadratic formula is positive. Meaning it has two solutions.
// This means that the ray intersects the sphere at two points, an entry and exit point.
float sphere_hit_distance(vec3 rayOrigin, vec3 rayDirection, vec3 sphereOrigin, float sphereRadius) {
    // t² * dot(B,B) + 2t * dot(B, A-C) + dot(A - C, A - C) - r² = 0;
    // at² + bt + c = 0
    // B = rayDirection, A = rayOrigin, C = sphereCenter, r = sphereRadius
    float a = dot(rayDirection, rayDirection);
    float b = 2 * dot(rayDirection, rayOrigin - sphereOrigin);
    float c = dot(rayOrigin - sphereOrigin, rayOrigin - sphereOrigin) - sphereRadius*sphereRadius;

    float x = b*b - 4*a*c;

    if (x < 0) {
        return -1.0;
    } else {
        return (-b - sqrt(x)) / (2.0 * a);
    }
}

float box_hit_distance(vec3 rayOrigin, vec3 rayDirection, vec3 minimumBounds, vec3 maximumBounds) {
    vec3 inverse = vec3(1.0) / rayDirection;
    vec3 bottom = inverse * (minimumBounds - rayOrigin);
    vec3 top = inverse * (maximumBounds - rayOrigin);

    vec3 minimum = min(top, bottom);
    vec3 maximum = max(top, bottom);

    float t0 = max(max(minimum.x, minimum.y), max(minimum.x, minimum.z));
    float t1 = min(min(maximum.x, maximum.y), min(maximum.x, maximum.z));

    if (t1 < t0) {
        return -1.0;
    } else {
        return t0;
    }
}

float plane_hit_distance(vec3 rayOrigin, vec3 rayDirection, vec3 center, vec3 normal) {
    // t = ((center - rayOrigin) dot normal) / rayDirection dot normal
    if (dot(rayDirection, normal) > 0) {
        return -1.0f;
    }

    float numerator = dot(center - rayOrigin, normal);
    float denominator = dot(rayDirection, normal);

    float parallelCheck = 0.00001; 
    if (abs(denominator) > parallelCheck) {
        float t = numerator / denominator;
        if (t >= 0) {
            return t;
        } else {
            return -1.0f;
        }
    } else {
        return -1.0f;
    }
}


bool inShadow(vec3 hitPoint, vec3 normal, vec3 lightDirection, float lightDistance) {
    float shadowRayOffset = 0.001;
    vec3 offsetHitPoint = hitPoint + normal * shadowRayOffset;

    for (int i = 0; i < sphereCount; i++) {
        float blockDistance = sphere_hit_distance(offsetHitPoint, lightDirection, spheres[i].origin, spheres[i].radius);
        if (blockDistance >= 0 && blockDistance <= lightDistance) {
            return true;
        }
    }
    for (int i = 0; i < boxCount; i++) {
        float blockDistance = box_hit_distance(offsetHitPoint, lightDirection, boxes[i].minimumBounds, boxes[i].maximumBounds);
        if (blockDistance >= 0 && blockDistance <= lightDistance) {
            return true;
        }
    }
    for (int i = 0; i < planeCount; i++) {
        float blockDistance = plane_hit_distance(offsetHitPoint, lightDirection, planes[i].center, planes[i].normal);
        if (blockDistance >= 0 && blockDistance <= 1) {
            return true;
        }
    }

    return false;
}


vec4 calculateLighting(vec3 rayDirection, vec3 hitPoint, vec3 normal, vec3 objectColor, float reflectivity, float specularFalloff) {
    vec3 ambient = ambientColor * ambientIntensity;
    vec3 diffuse = vec3(0.0);
    vec3 specular = vec3(0.0);

    for (int i = 0; i < lightCount; i++) {
        Light light = lights[i];
        vec3 lightDirection = normalize(light.position - hitPoint);
        float lightDistance = distance(hitPoint, light.position);

        if (inShadow(hitPoint, normal, lightDirection, lightDistance)) {
            continue;
        }

        float attenuation = 1.0 / (attenConstant + attenLinear * lightDistance + attenQuadratic * lightDistance * lightDistance);
        
        float cosDiff = dot(lightDirection, normal);
        cosDiff = max(cosDiff, 0);

        diffuse += (light.color * objectColor) * cosDiff * light.intensity * attenuation;

        if (specularFalloff > 0) {
            vec3 halfVector = normalize(lightDirection + -rayDirection);
            float cosSpec = dot(halfVector, normal);
            cosSpec = max(cosSpec, 0);
            specular += vec3(reflectivity * pow(cosSpec, specularFalloff)) * light.intensity * attenuation;
        }
    }

    return vec4(diffuse + specular + ambient, 1.0);
}


void calculateSpheres(vec3 rayOrigin, vec3 rayDirection, inout float nearestDistance, inout vec3 nearestHitPoint, inout vec3 normal, inout vec4 color, inout float reflectivity) {
    for (int i = 0; i < sphereCount; i++) {
        Sphere sphere = spheres[i];

        float hitDistance = sphere_hit_distance(rayOrigin, rayDirection, sphere.origin, sphere.radius);
        if (hitDistance >= 0 && hitDistance <= nearestDistance) {
            nearestDistance = hitDistance;

            vec3 hitPoint = rayOrigin + rayDirection*hitDistance;
            normal = normalize(hitPoint - sphere.origin);

            nearestHitPoint = hitPoint;

            color = calculateLighting(rayDirection, hitPoint, normal, sphere.color, sphere.reflectivity, sphere.specularFalloff); 
            reflectivity = sphere.reflectivity;
        }
    }
}


void calculateBoxes(vec3 rayOrigin, vec3 rayDirection, inout float nearestDistance, inout vec3 nearestHitPoint, inout vec3 normal, inout vec4 color, inout float reflectivity) {
    for (int i = 0; i < boxCount; i++) {
        Box box = boxes[i];

        float hitDistance = box_hit_distance(rayOrigin, rayDirection, box.minimumBounds, box.maximumBounds);
        if (hitDistance >= 0 && hitDistance <= nearestDistance) {
            nearestDistance = hitDistance;

            vec3 hitPoint = rayOrigin + rayDirection*hitDistance;
            normal = vec3(0.0);

            nearestHitPoint = hitPoint;

            float error = 0.00001;

            if (abs(hitPoint.x - box.minimumBounds.x) < error) { normal = vec3(-1.0, 0.0, 0.0); }
            else if (abs(hitPoint.x - box.maximumBounds.x) < error) { normal = vec3(1.0, 0.0, 0.0); }
            else if (abs(hitPoint.y - box.minimumBounds.y) < error) { normal = vec3(0.0, -1.0, 0.0); }
            else if (abs(hitPoint.y - box.maximumBounds.y) < error) { normal = vec3(0.0, 1.0, 0.0); }
            else if (abs(hitPoint.z - box.minimumBounds.z) < error) { normal = vec3(0.0, 0.0, -1.0); }
            else if (abs(hitPoint.z - box.maximumBounds.z) < error) { normal = vec3(0.0, 0.0, 1.0); }

            color = calculateLighting(rayDirection, hitPoint, normal, box.color, box.reflectivity, box.specularFalloff);
            reflectivity = box.reflectivity;
        }
    }
}


void calculatePlanes(vec3 rayOrigin, vec3 rayDirection, inout float nearestDistance, inout vec3 nearestHit, inout vec3 normal, inout vec4 color, inout float reflectivity) {
    for (int i = 0; i < planeCount; i++) {
        Plane plane = planes[i];
        
        float hitDistance = plane_hit_distance(rayOrigin, rayDirection, plane.center, plane.normal);
        if (hitDistance >= 0 && hitDistance <= nearestDistance) {
            nearestDistance = hitDistance;

            vec3 hitPoint = rayOrigin + rayDirection*hitDistance;
            nearestHit = hitPoint;
            normal = plane.normal;
            color = calculateLighting(rayDirection, hitPoint, plane.normal, plane.color, plane.reflectivity, plane.specularFalloff);
            reflectivity = plane.reflectivity;
        }
    }
}


vec4 getFragColor(vec3 rayOrigin, vec3 rayDirection) {
    float nearestDistance = 1.0 / 0.0;
    vec3 nearestHitPoint = vec3(1.0);
    vec3 normal = vec3(0.0);
    vec4 color = vec4(0.0);
    float reflectivity = 0.0;

    calculateSpheres(rayOrigin, rayDirection, nearestDistance, nearestHitPoint, normal, color, reflectivity);
    calculateBoxes(rayOrigin, rayDirection, nearestDistance, nearestHitPoint, normal, color, reflectivity);
    calculatePlanes(rayOrigin, rayDirection, nearestDistance, nearestHitPoint, normal, color, reflectivity);


    gl_FragDepth = calcDepth(nearestHitPoint);

    vec4 reflectedColor = vec4(0.0);
    vec3 reflectionPoint = nearestHitPoint;
    vec3 reflectionNormal = normal;
    float cumulativeReflectivity = 1.0;

    float reflectionDecay = 1.0f;
    for (int i = 0; i < reflectionCount; i++) {
        vec3 reflectionRayDirection = rayDirection - 2 * dot(reflectionNormal, rayDirection) * reflectionNormal;
        vec3 reflectionRayOrigin = reflectionPoint + reflectionNormal * 0.001;

        float rDist = 1.0 / 0.0;
        vec3 rHit = vec3(0.0);
        vec3 rNorm = vec3(0.0);
        vec4 rColor = vec4(0.0);
        float rRef = 0.0;

        calculateSpheres(reflectionRayOrigin, reflectionRayDirection, rDist, rHit, rNorm, rColor, rRef);
        calculateBoxes(reflectionRayOrigin, reflectionRayDirection, rDist, rHit, rNorm, rColor, rRef);
        calculatePlanes(reflectionRayOrigin, reflectionRayDirection, rDist, rHit, rNorm, rColor, rRef);

        reflectedColor += cumulativeReflectivity * reflectionDecay * rColor;

        reflectionPoint = rHit;
        reflectionNormal = rNorm;
        rayDirection = reflectionRayDirection;

        cumulativeReflectivity *= rRef;
        reflectionDecay *= reflectionDecayRatio;

        if (cumulativeReflectivity < 0.01 || reflectionDecay < 0.01) {
            break;
        }
    }

    color += mix(color, reflectedColor, reflectivity);
    return color;
}

void main()
{
	vec4 near = inverseModelViewProjectionMatrix*vec4(fragPosition,-1.0,1.0);
	near /= near.w;

	vec4 far = inverseModelViewProjectionMatrix*vec4(fragPosition,1.0,1.0);
	far /= far.w;

	// this is the setup for our viewing ray
	vec3 rayOrigin = near.xyz;
	vec3 rayDirection = normalize((far-near).xyz);

    gl_FragDepth = 1.0;
	// using calcDepth, you can convert a ray position to an OpenGL z-value, so that intersections/occlusions with the
	// model geometry are handled correctly, e.g.: gl_FragDepth = calcDepth(nearestHit);
	// in case there is no intersection, you should get gl_FragDepth to 1.0, i.e., the output of the shader will be ignored

    fragColor = getFragColor(rayOrigin, rayDirection);
    
    // Render lights
    for (int i = 0; i < lightCount; i++) {
        Light light = lights[i];
        float hitDistance = sphere_hit_distance(rayOrigin, rayDirection, light.position, 0.25);
        if (hitDistance >= 0) {
            vec3 hitPoint = rayOrigin + rayDirection*hitDistance;
            fragColor = vec4(light.color, 1.0);
            gl_FragDepth = calcDepth(hitPoint);
        }
    }
}
