#version 400
#extension GL_ARB_shading_language_include : require
#include "/model-globals.glsl"

uniform vec3 worldCameraPosition;

uniform bool toonEnabled;
uniform int toonShadeCount;

uniform vec3 worldLightPosition;
uniform float worldLightIntensity;
uniform vec3 worldLightColor;
uniform float attenA;
uniform float attenB;
uniform float attenC;

uniform float ambientLightIntensity;
uniform vec3 ambientLightColor;
uniform vec3 ambientReflectance;
uniform sampler2D ambientTexture;
uniform bool hasAmbientTexture;
uniform bool ambientEnabled;

uniform vec3 diffuseReflectance;
uniform sampler2D diffuseTexture;
uniform bool hasDiffuseTexture;
uniform bool diffuseEnabled;

uniform vec3 specularReflectance;
uniform float shininess;
uniform float shininessMultiplier;
uniform sampler2D specularTexture;
uniform bool hasSpecularTexture;
uniform bool specularEnabled;

uniform sampler2D objectNormalsTexture;
uniform bool hasObjectNormalsTexture;

uniform sampler2D tangentNormalTexture;
uniform bool hasTangentNormalTexture;

uniform bool wireframeEnabled;
uniform vec4 wireframeLineColor;

uniform float bumpAmplitude;
uniform float bumpPeriod;

uniform int whichNormalMap;

in fragmentData
{
	vec3 position;
	vec3 normal;
	vec2 texCoord;
	noperspective vec3 edgeDistance;
    vec3 tangent;
    vec3 bitangent;
} fragment;

out vec4 fragColor;


float bump(float u, float v, float A, float k) {
    return A * pow(sin(k * u), 2.0) * pow(sin(k * v), 2.0);
}


vec4 blinnPhong() {
    float fragDistance = distance(worldLightPosition, fragment.position);
    float attenuation = 1.0 / (attenA + attenB * fragDistance + attenC * pow(fragDistance, 2));
    float lightIntensity = worldLightIntensity * attenuation;

    // Set up commonly used directions
    vec3 lightDirection = normalize(worldLightPosition - fragment.position);
    vec3 viewDirection = normalize(worldCameraPosition - fragment.position);
    vec3 normalizedFragmentNormal = normalize(fragment.normal);

    if (whichNormalMap == 1) {
        if (hasObjectNormalsTexture) {
            normalizedFragmentNormal = texture(objectNormalsTexture, fragment.texCoord).rgb;
            normalizedFragmentNormal = normalize(normalizedFragmentNormal * 2.0 - 1);
        }
    } else if (whichNormalMap == 2) {
        if (hasTangentNormalTexture) {
            vec3 n = texture(tangentNormalTexture, fragment.texCoord).rgb;
            n = normalize(n * 2.0 - 1);

            mat3 TBN = mat3(fragment.tangent, fragment.bitangent, normalizedFragmentNormal);
            normalizedFragmentNormal = normalize(TBN * n);
        }
    } else if (whichNormalMap == 3) {
        float b = bump(fragment.texCoord.x, fragment.texCoord.y, bumpAmplitude, bumpPeriod);
        float u = fragment.texCoord.x;
        float v = fragment.texCoord.y;
        float bu = dFdx(b); 
        float bv = dFdy(b); 

        vec3 Pu = normalize(fragment.tangent);         
        vec3 Pv = normalize(fragment.bitangent);      

        normalizedFragmentNormal += bv * cross(Pu, normalizedFragmentNormal) + bu * cross(normalizedFragmentNormal, Pv);
        normalizedFragmentNormal = normalize(normalizedFragmentNormal);
    }

    // Diffuse reflection calulation. Make sure cos is positive
    // as negative values means its behind the object
    float cosDiff = dot(lightDirection, normalizedFragmentNormal);
    cosDiff = max(cosDiff, 0);
    
    vec3 diffuseLight;
    if (hasDiffuseTexture && diffuseEnabled) {
        vec4 texValue = texture(diffuseTexture, fragment.texCoord);
        diffuseLight = (worldLightColor * texValue.rgb) * cosDiff * lightIntensity;
    } else {
        diffuseLight = (worldLightColor * diffuseReflectance) * cosDiff * lightIntensity;
    }

    vec3 ambientLight;
    if (hasAmbientTexture && ambientEnabled) {
        vec4 texValue = texture(ambientTexture, fragment.texCoord);
        ambientLight = (ambientLightColor * texValue.rgb) * ambientLightIntensity;
    } else {
        ambientLight = (ambientLightColor * ambientReflectance) * ambientLightIntensity; 
    }


    // Calculate specular highlight. Make sure cos is positive 
    // as negative values means its behind the object
    vec3 specularHighlight;
    if (shininess > 0) {
        vec3 halfVector = normalize(lightDirection + viewDirection);
        float cosSpec = dot(halfVector, normalizedFragmentNormal);
        cosSpec = max(cosSpec, 0);

        if (hasSpecularTexture && specularEnabled) {
            vec4 texValue = texture(specularTexture, fragment.texCoord);
            specularHighlight = (worldLightColor * texValue.rgb * shininessMultiplier) * pow(cosSpec, shininess) * lightIntensity;
        } else {
            specularHighlight = (worldLightColor * specularReflectance * shininessMultiplier) * pow(cosSpec, shininess) * lightIntensity;
        }
    } 

    if (toonEnabled) {
        diffuseLight = round(diffuseLight * toonShadeCount) / toonShadeCount;
        ambientLight = round(ambientLight * toonShadeCount) / toonShadeCount;
        specularHighlight = round(specularHighlight * toonShadeCount) / toonShadeCount;
    }

    return vec4(ambientLight + diffuseLight + specularHighlight, 1.0);
}


void main()
{
    vec4 result = blinnPhong();


	if (wireframeEnabled)
	{
		float smallestDistance = min(min(fragment.edgeDistance[0],fragment.edgeDistance[1]),fragment.edgeDistance[2]);
		float edgeIntensity = exp2(-1.0*smallestDistance*smallestDistance);
		result.rgb = mix(result.rgb,wireframeLineColor.rgb,edgeIntensity*wireframeLineColor.a);
	}

	fragColor = result;
}


