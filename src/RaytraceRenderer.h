#pragma once
#include "Renderer.h"
#include <memory>

#include <glm/glm.hpp>
#include <glbinding/gl/gl.h>
#include <glbinding/gl/enum.h>
#include <glbinding/gl/functions.h>

#include <globjects/VertexArray.h>
#include <globjects/VertexAttributeBinding.h>
#include <globjects/Buffer.h>
#include <globjects/Program.h>
#include <globjects/Shader.h>
#include <globjects/Framebuffer.h>
#include <globjects/Renderbuffer.h>
#include <globjects/Texture.h>
#include <globjects/base/File.h>
#include <globjects/TextureHandle.h>
#include <globjects/NamedString.h>
#include <globjects/base/StaticStringSource.h>

namespace minity
{
	class Viewer;

	class RaytraceRenderer : public Renderer
	{
	public:
		RaytraceRenderer(Viewer *viewer);
		virtual void display();
        glm::vec3 interpolate(std::vector<glm::vec3> ps, float t);

	private:
		std::unique_ptr<globjects::VertexArray> m_quadArray = std::make_unique<globjects::VertexArray>();
		std::unique_ptr<globjects::Buffer> m_quadVertices = std::make_unique<globjects::Buffer>();
	};

    struct Light {
        glm::vec3 position = glm::vec3(0.f, 0.f, 0.f);
        glm::vec3 color = glm::vec3(1.f, 1.f, 1.f);
        float intensity = 1.0f;
    };

    struct Sphere {
        glm::vec3 origin = glm::vec3(0.f);
        float radius = 1.f;
        glm::vec3 color = glm::vec3(1.f, 0.f, 0.f);
        float reflectivity = 0.5f;
        float specularFalloff = 10.f;
    };

    struct Box {
        glm::vec3 minimumBounds = glm::vec3(0.0);
        glm::vec3 maximumBounds = glm::vec3(1.0);
        glm::vec3 color = glm::vec3(0.f, 1.f, 0.f);
        float reflectivity = 0.5f;
        float specularFalloff = 10.f;
    };
    
    struct Plane {
        glm::vec3 center = glm::vec3(0.0);
        glm::vec3 normal = glm::vec3(0.0, 1.0, 0.0);
        glm::vec3 color = glm::vec3(0.f, 1.f, 0.f);
        float reflectivity = 0.5f;
        float specularFalloff = 10.f;
    };
}
