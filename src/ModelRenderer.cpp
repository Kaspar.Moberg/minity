#include "ModelRenderer.h"
#include <bits/chrono.h>
#include <cmath>
#include <cstdio>
#include <glbinding/gl/enum.h>
#include <glbinding/gl/functions.h>
#include <glm/fwd.hpp>
#include <globjects/base/File.h>
#include <globjects/State.h>
#include <globjects/base/baselogging.h>
#include <iostream>
#include <filesystem>
#include <imgui.h>
#include "Viewer.h"
#include "Scene.h"
#include "Model.h"
#include <math.h>
#include <ostream>
#include <sstream>
#include <algorithm>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vector>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>

using namespace minity;
using namespace gl;
using namespace glm;
using namespace globjects;


ModelRenderer::ModelRenderer(Viewer* viewer) : Renderer(viewer)
{
	m_lightVertices->setStorage(std::array<vec3, 1>({ vec3(0.0f) }), GL_NONE_BIT);
	auto lightVertexBinding = m_lightArray->binding(0);
	lightVertexBinding->setBuffer(m_lightVertices.get(), 0, sizeof(vec3));
	lightVertexBinding->setFormat(3, GL_FLOAT);
	m_lightArray->enable(0);
	m_lightArray->unbind();

	createShaderProgram("model-base", {
		{ GL_VERTEX_SHADER,"./res/model/model-base-vs.glsl" },
		{ GL_GEOMETRY_SHADER,"./res/model/model-base-gs.glsl" },
		{ GL_FRAGMENT_SHADER,"./res/model/model-base-fs.glsl" },
		}, 
		{ "./res/model/model-globals.glsl" });

	createShaderProgram("model-light", {
		{ GL_VERTEX_SHADER,"./res/model/model-light-vs.glsl" },
		{ GL_FRAGMENT_SHADER,"./res/model/model-light-fs.glsl" },
		}, { "./res/model/model-globals.glsl" });
}

void ModelRenderer::display()
{
	// Save OpenGL state
	auto currentState = State::currentState();

	// retrieve/compute all necessary matrices and related properties
	const mat4 viewMatrix = viewer()->viewTransform();
	const mat4 inverseViewMatrix = inverse(viewMatrix);
	const mat4 modelViewMatrix = viewer()->modelViewTransform();
	const mat4 inverseModelViewMatrix = inverse(modelViewMatrix);
	const mat4 modelLightMatrix = viewer()->modelLightTransform();
	const mat4 inverseModelLightMatrix = inverse(modelLightMatrix);
	const mat4 modelViewProjectionMatrix = viewer()->modelViewProjectionTransform();
	const mat4 inverseModelViewProjectionMatrix = inverse(modelViewProjectionMatrix);
	const mat4 projectionMatrix = viewer()->projectionTransform();
	const mat4 inverseProjectionMatrix = inverse(projectionMatrix);
	const mat3 normalMatrix = mat3(transpose(inverseModelViewMatrix));
	const mat3 inverseNormalMatrix = inverse(normalMatrix);
	const vec2 viewportSize = viewer()->viewportSize();

	auto shaderProgramModelBase = shaderProgram("model-base");

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	viewer()->scene()->model()->vertexArray().bind();

	const std::vector<Group> & groups = viewer()->scene()->model()->groups();
    std::vector<Vertex> vertices = viewer()->scene()->model()->vertices();
    const std::vector<uint> &indicies = viewer()->scene()->model()->indices();
    const glm::vec3 modelCenter = viewer()->scene()->model()->center();
	const std::vector<Material> & materials = viewer()->scene()->model()->materials();

	static std::vector<bool> groupEnabled(groups.size(), true);
	static bool wireframeEnabled = true;
	static bool lightSourceEnabled = true;
	static vec4 wireframeLineColor = vec4(1.0f);

    // Lighting variables
    static vec3 worldLightColor = vec3(1.0f, 1.0f, 1.0f);
    static float worldLightIntensity = 1.0f;
    static float attenA = 1.0;
    static float attenB = 0.1;
    static float attenC = 0.01;

    static vec3 ambientLightColor = vec3(0.1f, 0.08f, 0.06f);
    static float ambientLightIntensity = 0.2f;

    static float shininessMultiplier = 1.0f;

    static int whichNormalMap = 0; // 0 for object 1 for tangent 
    static float bumpAmplitude = 5;
    static float bumpPeriod = 50;

    static bool diffuseEnabled = false;
    static bool ambientEnabled = false;
    static bool specularEnabled = false;

    static bool toonEnabled = false;
    static int toonShadeCount = 5;

    float &explosionDistance = viewer()->getExplosionDistance();

	if (ImGui::BeginMenu("Model"))
	{
		ImGui::Checkbox("Wireframe Enabled", &wireframeEnabled);
		ImGui::Checkbox("Light Source Enabled", &lightSourceEnabled);

        ImGui::Checkbox("Toon Shading Enabled", (bool*)&toonEnabled);
        if (toonEnabled) {
            ImGui::SliderInt("Toon Shade Count", (int*)&toonShadeCount, 0, 10);
        }

		ImGui::ColorEdit3("Light Color", (float*)&worldLightColor);
        ImGui::SliderFloat("Light Intensity", (float*)&worldLightIntensity, 0.0f, 100.0f);

        ImGui::ColorEdit3("Ambient Light Color", (float*)&ambientLightColor);
        ImGui::SliderFloat("Ambient Light Intensity", (float*)&ambientLightIntensity, 0.0f, 100.0f);
        ImGui::SliderFloat("Shininess Multiplier", (float*)&shininessMultiplier, 1.0f, 100.0f);

        ImGui::RadioButton("No Normal Map", &whichNormalMap, 0);
        ImGui::RadioButton("Object Normal Map", &whichNormalMap, 1);
        ImGui::RadioButton("Tangent Normal Map", &whichNormalMap, 2);
        ImGui::RadioButton("Bump Normal Map", &whichNormalMap, 3);

        if (whichNormalMap == 3) {
            ImGui::SliderFloat("Bump Amplitude", (float*)&bumpAmplitude, 0, 10);
            ImGui::SliderFloat("Bump Period", (float*)&bumpPeriod, 0, 100);
        }

        if (ImGui::CollapsingHeader("Textures")) {
            ImGui::Checkbox("Diffuse", (bool *)&diffuseEnabled);
            ImGui::Checkbox("Ambient", (bool *)&ambientEnabled);
            ImGui::Checkbox("Specular", (bool *)&specularEnabled);
        }

        if (ImGui::CollapsingHeader("Attenuation")) {
            ImGui::SliderFloat("Atten A", &attenA, 0, 1);
            ImGui::SliderFloat("Atten B", &attenB, 0, 1);
            ImGui::SliderFloat("Atten C", &attenC, 0, 1);
        }

        if (ImGui::CollapsingHeader("Animation")) {
            if (ImGui::SliderFloat("Explosion Rate", (float*)&explosionDistance, 0, 5) || !viewer()->animate()) {
                for (auto &g : groups) {
                    glm::vec3 movement = glm::normalize(g.center - modelCenter) * explosionDistance;

                    for(auto i : g.vertexIndices) {
                        Vertex &v = vertices.at(i);
                        v.position += movement;
                    }
                }
                viewer()->scene()->model()->vertexBuffer().setSubData(vertices);
            }
        } 

		if (wireframeEnabled)
		{
			if (ImGui::CollapsingHeader("Wireframe"))
			{
				ImGui::ColorEdit4("Line Color", (float*)&wireframeLineColor, ImGuiColorEditFlags_AlphaBar);
			}
		}

		if (ImGui::CollapsingHeader("Groups"))
		{
			for (uint i = 0; i < groups.size(); i++)
			{
				bool checked = groupEnabled.at(i);
				ImGui::Checkbox(groups.at(i).name.c_str(), &checked);
				groupEnabled[i] = checked;
			}
		}

		ImGui::EndMenu();
	}

	vec4 worldCameraPosition = inverseModelViewMatrix * vec4(0.0f, 0.0f, 0.0f, 1.0f);
	vec4 worldLightPosition = inverseModelLightMatrix * vec4(0.0f, 0.0f, 0.0f, 1.0f); 
    
	shaderProgramModelBase->setUniform("modelViewProjectionMatrix", modelViewProjectionMatrix);
	shaderProgramModelBase->setUniform("viewportSize", viewportSize);
	shaderProgramModelBase->setUniform("worldCameraPosition", vec3(worldCameraPosition));
	shaderProgramModelBase->setUniform("worldLightPosition", vec3(worldLightPosition));
	shaderProgramModelBase->setUniform("wireframeEnabled", wireframeEnabled);
	shaderProgramModelBase->setUniform("wireframeLineColor", wireframeLineColor);
	
	shaderProgramModelBase->use();

	for (uint i = 0; i < groups.size(); i++)
	{
		if (groupEnabled.at(i))
		{
			const Material & material = materials.at(groups.at(i).materialIndex);

			shaderProgramModelBase->setUniform("diffuseReflectance", material.diffuse);

            shaderProgramModelBase->setUniform("worldLightColor", worldLightColor);
            shaderProgramModelBase->setUniform("worldLightIntensity", worldLightIntensity);
            shaderProgramModelBase->setUniform("attenA", attenA);
            shaderProgramModelBase->setUniform("attenB", attenB);
            shaderProgramModelBase->setUniform("attenC", attenC);

            shaderProgramModelBase->setUniform("ambientReflectance", material.ambient);
            shaderProgramModelBase->setUniform("ambientLightColor", ambientLightColor);
            shaderProgramModelBase->setUniform("ambientLightIntensity", ambientLightIntensity);
            shaderProgramModelBase->setUniform("shininessMultiplier", shininessMultiplier);
            shaderProgramModelBase->setUniform("shininess", material.shininess);
            shaderProgramModelBase->setUniform("whichNormalMap", whichNormalMap);
            shaderProgramModelBase->setUniform("specularReflectance", material.specular);
            shaderProgramModelBase->setUniform("toonEnabled", toonEnabled);
            shaderProgramModelBase->setUniform("toonShadeCount", toonShadeCount);
            shaderProgramModelBase->setUniform("bumpAmplitude", bumpAmplitude);
            shaderProgramModelBase->setUniform("bumpPeriod", bumpPeriod);
            shaderProgramModelBase->setUniform("diffuseEnabled", diffuseEnabled);
            shaderProgramModelBase->setUniform("ambientEnabled", ambientEnabled);
            shaderProgramModelBase->setUniform("specularEnabled", specularEnabled);

			if (material.diffuseTexture)
			{
				shaderProgramModelBase->setUniform("diffuseTexture", 0);
                shaderProgramModelBase->setUniform("hasDiffuseTexture", true);
				material.diffuseTexture->bindActive(0);
			}
            if (material.specularTexture) {
				shaderProgramModelBase->setUniform("specularTexture", 1);
                shaderProgramModelBase->setUniform("hasSpecularTexture", true);
				material.specularTexture->bindActive(1);
            }
            if (material.ambientTexture) {
				shaderProgramModelBase->setUniform("ambientTexture", 2);
                shaderProgramModelBase->setUniform("hasAmbientTexture", true);
				material.ambientTexture->bindActive(2);
            }
            if (material.objectNormalTexture) {
				shaderProgramModelBase->setUniform("objectNormalsTexture", 3);
                shaderProgramModelBase->setUniform("hasObjectNormalsTexture", true);
				material.objectNormalTexture->bindActive(3);
            }
            if (material.tangentNormalTexture) {
				shaderProgramModelBase->setUniform("tangentNormalTexture", 4);
                shaderProgramModelBase->setUniform("hasTangentNormalTexture", true);
				material.tangentNormalTexture->bindActive(4);
            }

			viewer()->scene()->model()->vertexArray().drawElements(GL_TRIANGLES, groups.at(i).count(), GL_UNSIGNED_INT, (void*)(sizeof(GLuint)*groups.at(i).startIndex));

			if (material.diffuseTexture)
			{
				material.diffuseTexture->unbind();
			}
			if (material.specularTexture)
			{
				material.specularTexture->unbind();
			}
			if (material.ambientTexture)
			{
				material.ambientTexture->unbind();
			}
			if (material.objectNormalTexture)
			{
				material.objectNormalTexture->unbind();
			}
			if (material.tangentNormalTexture)
			{
				material.tangentNormalTexture->unbind();
			}
		}
	}

	shaderProgramModelBase->release();

	viewer()->scene()->model()->vertexArray().unbind();


	if (lightSourceEnabled)
	{
		auto shaderProgramModelLight = shaderProgram("model-light");
		shaderProgramModelLight->setUniform("modelViewProjectionMatrix", modelViewProjectionMatrix * inverseModelLightMatrix);
		shaderProgramModelLight->setUniform("viewportSize", viewportSize);

		glEnable(GL_PROGRAM_POINT_SIZE);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(GL_FALSE);

		m_lightArray->bind();

		shaderProgramModelLight->use();
		m_lightArray->drawArrays(GL_POINTS, 0, 1);
		shaderProgramModelLight->release();

		m_lightArray->unbind();

		glDisable(GL_PROGRAM_POINT_SIZE);
		glDisable(GL_BLEND);
		glDepthMask(GL_TRUE);
	}

	// Restore OpenGL state (disabled to to issues with some Intel drivers)
	// currentState->apply(); 

    if (viewer()->animate()) {
        auto animationT = viewer()->getAnimationT();

        if (animationT >= 1) {
            viewer()->stopAnimation();
        } 
        
        std::vector<Frame> &frames = viewer()->getFrames();

        std::vector<float> explosionKeyframes = std::vector<float>{
            frames[0].explodedRate,
            frames[1].explodedRate,
            frames[2].explodedRate,
            frames[3].explodedRate,
        };
        std::vector<mat4> cameraKeyframes = std::vector<mat4>{
            frames[0].viewTransform,
            frames[1].viewTransform,
            frames[2].viewTransform,
            frames[3].viewTransform,
        };
        std::vector<mat4> lightKeyframes = std::vector<mat4>{
            frames[0].lightTransform,
            frames[1].lightTransform,
            frames[2].lightTransform,
            frames[3].lightTransform,
        };
        std::vector<vec3> backgroundColorKeyframes = std::vector<vec3>{
            frames[0].backgroundColor,
            frames[1].backgroundColor,
            frames[2].backgroundColor,
            frames[3].backgroundColor,
        };

        explosionDistance = interpolate(explosionKeyframes, animationT);
        for (auto &g : groups) {
            glm::vec3 movement = glm::normalize(g.center - modelCenter) * explosionDistance;

            for(auto i : g.vertexIndices) {
                Vertex &v = vertices.at(i);
                v.position += movement;
            }
        }
        viewer()->scene()->model()->vertexBuffer().setSubData(vertices);
        viewer()->setViewTransform(interpolate(cameraKeyframes, animationT));
        viewer()->setLightTransform(interpolate(lightKeyframes, animationT));
        viewer()->setBackgroundColor(interpolate(backgroundColorKeyframes, animationT));
    }
}

float ModelRenderer::interpolate(std::vector<float> ps, float t) {
    float f0 = 2.0f * ps[1];
    float f1 = (-ps[0] + ps[2]) * t;
    float f2 = (2.0f * ps[0] - 5.0f * ps[1] + 4.0f * ps[2] - ps[3]) * pow(t, 2);
    float f3 = (-ps[0] + 3.0f * ps[1] - 3.0f * ps[2] + ps[3]) * pow(t, 3);
    
    return 0.5f * (f0 + f1 + f2 + f3);
}

vec3 ModelRenderer::interpolate(std::vector<vec3> ps, float t) {
    vec3 v0 = 2.0f * ps[1];
    vec3 v1 = (-ps[0] + ps[2]) * t;
    vec3 v2 = (2.0f * ps[0] - 5.0f * ps[1] + 4.0f * ps[2] - ps[3]) * static_cast<float>(pow(t, 2));
    vec3 v3 = (-ps[0] + 3.0f * ps[1] - 3.0f * ps[2] + ps[3]) * static_cast<float>(pow(t, 3));
    
    return 0.5f * (v0 + v1 + v2 + v3);
}

quat ModelRenderer::interpolate(std::vector<quat> ps, float t) {
    quat q0 = 2.0f * ps[1];
    quat q1 = (-ps[0] + ps[2]) * t;
    quat q2 = (2.0f * ps[0] - 5.0f * ps[1] + 4.0f * ps[2] - ps[3]) * static_cast<float>(pow(t, 2));
    quat q3 = (-ps[0] + 3.0f * ps[1] - 3.0f * ps[2] + ps[3]) * static_cast<float>(pow(t, 3));
    
    return 0.5f * (q0 + q1 + q2 + q3);
}

// TODO: Extract rotation, scaling, and transformation
mat4 ModelRenderer::interpolate(std::vector<mat4> ps, float t) {
    vec3 t0, s0;
    mat4 r0;
    matrixDecompose(ps[0], t0, r0, s0);

    vec3 t1, s1;
    mat4 r1;
    matrixDecompose(ps[1], t1, r1, s1);

    vec3 t2, s2;
    mat4 r2;
    matrixDecompose(ps[2], t2, r2, s2);

    vec3 t3, s3;
    mat4 r3;
    matrixDecompose(ps[3], t3, r3, s3);

    std::vector<quat> qs = std::vector<quat>{
        quat_cast(r0),
        quat_cast(r1),
        quat_cast(r2),
        quat_cast(r3),
    };


    vec3 interpolatedT = interpolate(std::vector<vec3>{t0, t1, t2, t3}, t);
    vec3 interpolatedS = interpolate(std::vector<vec3>{s0, s1, s2, s3}, t);
    mat4 interpolatedR = mat4_cast(interpolate(qs, t));

    mat4 mt = mat4{
        1, 0, 0, interpolatedT.x,
        0, 1, 0, interpolatedT.y,
        0, 0, 1, interpolatedT.z,
        0, 0, 0, 1,
    };
    mat4 ms = mat4{
        interpolatedS.x, 0, 0, 0,
        0, interpolatedS.y, 0, 0,
        0, 0, interpolatedS.z, 0,
        0, 0, 0, 1
    };

    return mt * ms * interpolatedR;
}
