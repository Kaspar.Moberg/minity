#include "RaytraceRenderer.h"
#include <bits/chrono.h>
#include <charconv>
#include <chrono>
#include <glbinding/gl/bitfield.h>
#include <glbinding/gl/enum.h>
#include <glbinding/gl/functions.h>
#include <glbinding/gl/values.h>
#include <glm/fwd.hpp>
#include <globjects/base/File.h>
#include <globjects/State.h>
#include <globjects/base/baselogging.h>
#include <iostream>
#include <filesystem>
#include <imgui.h>
#include "Viewer.h"
#include "Scene.h"
#include "Model.h"
#include <sstream>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <string>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>

using namespace minity;
using namespace gl;
using namespace glm;
using namespace globjects;

RaytraceRenderer::RaytraceRenderer(Viewer* viewer) : Renderer(viewer)
{
	m_quadVertices->setStorage(std::array<vec2, 4>({ vec2(-1.0f, 1.0f), vec2(-1.0f,-1.0f), vec2(1.0f,1.0f), vec2(1.0f,-1.0f) }), gl::GL_NONE_BIT);
	auto vertexBindingQuad = m_quadArray->binding(0);
	vertexBindingQuad->setBuffer(m_quadVertices.get(), 0, sizeof(vec2));
	vertexBindingQuad->setFormat(2, GL_FLOAT);
	m_quadArray->enable(0);
	m_quadArray->unbind();

	createShaderProgram("raytrace", {
			{ GL_VERTEX_SHADER,"./res/raytrace/raytrace-vs.glsl" },
			{ GL_FRAGMENT_SHADER,"./res/raytrace/raytrace-fs.glsl" },
		}, 
		{ "./res/raytrace/raytrace-globals.glsl" });
}

void RaytraceRenderer::display()
{
	// Save OpenGL state
	auto currentState = State::currentState();

	// retrieve/compute all necessary matrices and related properties
	const mat4 modelViewProjectionMatrix = viewer()->modelViewProjectionTransform();
	const mat4 inverseModelViewProjectionMatrix = inverse(modelViewProjectionMatrix);

	auto shaderProgramRaytrace = shaderProgram("raytrace");

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

    // Define objects
    static vec3 ambientColor = vec3(1.0f, 1.0f, 1.0f);
    static float ambientIntensity = 0.01f;

    static float attenConstant = 1.0f;
    static float attenLinear = 0.09f;
    static float attenQuadratic = 0.032;

    static int reflectionCount = 20;
    static float reflectionDecayRatio = 0.8f;

    static std::vector<Sphere> spheres = std::vector<Sphere>{
        Sphere{glm::vec3(0.f, 0.f, 0.f), 1.f, glm::vec3(1.f, 0.f, 0.f), 0.1f, 10.f},
        Sphere{glm::vec3(-.5f, -1.4f, -.5f), 0.625f, glm::vec3(1.f, 1.f, 1.f), 1.f, 100.f},
    };
    static std::vector<Box> boxes = std::vector<Box>{
        Box{glm::vec3(-4.0), glm::vec3(-3.0), glm::vec3(0.f, 1.f, 0.f), 0.0f, 10.f},
        Box{glm::vec3(1.f, -5.f, 1.f), glm::vec3(-2.f, -2.f, -2.f), glm::vec3(0.f, .5f, 1.f), 0.0f, 10.f},
    };
    static std::vector<Plane> planes = std::vector<Plane>{
        // Top and bottom of box
        Plane{glm::vec3(0.f, -5.f, 0.f), glm::vec3(0.f, 1.f, 0.f), glm::vec3(0.75f), 0.0f, 10.f},
        Plane{glm::vec3(0.f, 5.f, 0.f), glm::vec3(0.f, -1.f, 0.f), glm::vec3(0.75f), 0.0f, 10.f},
        // Left and right of box
        Plane{glm::vec3(-5.f, 0.f, 0.f), glm::vec3(1.f, 0.f, 0.f), glm::vec3(0.75f), 0.9f, 10.f},
        Plane{glm::vec3(5.f, 0.f, 0.f), glm::vec3(-1.f, 0.f, 0.f), glm::vec3(0.75f), 0.9f, 10.f},
        // Back and front of box
        Plane{glm::vec3(0.f, 0.f, 5.f), glm::vec3(0.f, 0.f, -1.f), glm::vec3(0.75f), 0.0f, 10.f},
        Plane{glm::vec3(0.f, 0.f, -5.f), glm::vec3(0.f, 0.f, 1.f), glm::vec3(0.75f), 0.0f, 10.f},
    };
    static std::vector<Light> lights = std::vector<Light>{
        Light{glm::vec3(0.f, 4.5f, 0.f), glm::vec3(1.0), 0.5f},
        //Light{glm::vec3(-5.f, 0.f, -5.f), glm::vec3(1.0), .5f},
        //Light{glm::vec3(5.f, 5.f, 5.f), glm::vec3(1.0), .5f},
    };

    // UI
    if (ImGui::BeginMenu("Raytracer")) {
        if (ImGui::CollapsingHeader("Light Properties")) {
            ImGui::ColorEdit3("Ambient Light Color", (float*)&ambientColor);
            ImGui::SliderFloat("Ambient Light Intensity", (float*)&ambientIntensity, 0.0f, 1.0f);
            ImGui::SliderInt("Reflection Count", &reflectionCount, 0, 100);
            ImGui::SliderFloat("Reflection Decay Ratio", &reflectionDecayRatio, 0.f, 1.f);

            if (ImGui::CollapsingHeader("Attenuation")) {
                ImGui::SliderFloat("Constant", &attenConstant, 0.f, 1.f);
                ImGui::SliderFloat("Linear", &attenLinear, 0.f, 0.2f);
                ImGui::SliderFloat("Quadratic", &attenQuadratic, 0.f, 0.05f);
            }

            if (ImGui::CollapsingHeader("Lights")) {
                for (int i = 0; i < lights.size(); i++) {
                    std::string label = "Light " + std::to_string(i);
                    if (ImGui::CollapsingHeader(label.c_str())) {
                        ImGui::DragFloat3("Position", (float *)&lights[i].position, 0.1f);
                        ImGui::ColorEdit3("Color", (float *)&lights[i].color);
                        ImGui::SliderFloat("Intensity", &lights[i].intensity, 0.f, 10.f);
                    }
                }
            }
        }

        if (ImGui::CollapsingHeader("Objects")) {
            if (ImGui::CollapsingHeader("Spheres")) {
                for (int i = 0; i < spheres.size(); i++) {
                    std::string label = "Sphere " + std::to_string(i);
                    if (ImGui::CollapsingHeader(label.c_str())) {
                        ImGui::SliderFloat("Radius", &spheres[i].radius, 0.f, 10.f);
                        ImGui::DragFloat3("Origin", (float*)&spheres[i].origin, 0.1f);
                        ImGui::ColorEdit3("Color", (float*)&spheres[i].color);
                        ImGui::SliderFloat("Reflectivity", &spheres[i].reflectivity, 0.0f, 1.0f);
                        ImGui::SliderFloat("Specular Falloff", &spheres[i].specularFalloff, 10.0f, 100.0f);
                    }
                }
            }
            if (ImGui::CollapsingHeader("Boxes")) {
                for (int i = 0; i < boxes.size(); i++) {
                    std::string label = "Box " + std::to_string(i);
                    if (ImGui::CollapsingHeader(label.c_str())) {
                        ImGui::DragFloat3("MinimumBounds", (float*)&boxes[i].minimumBounds);
                        ImGui::DragFloat3("MaximumBounds", (float*)&boxes[i].maximumBounds);
                        ImGui::ColorEdit3("Color", (float*)&boxes[i].color);
                        ImGui::SliderFloat("Reflectivity", &boxes[i].reflectivity, 0.0f, 1.0f);
                        ImGui::SliderFloat("Specular Falloff", &boxes[i].specularFalloff, 10.0f, 100.0f);
                    }
                }
            }
            if (ImGui::CollapsingHeader("Planes")) {
                for (int i = 0; i < planes.size(); i++) {
                    std::string label = "Plane " + std::to_string(i);
                    if (ImGui::CollapsingHeader(label.c_str())) {
                        ImGui::DragFloat3("Center", (float*)&planes[i].center);
                        ImGui::DragFloat3("Normal", (float*)&planes[i].normal);
                        ImGui::ColorEdit3("Color", (float*)&planes[i].color);
                        ImGui::SliderFloat("Reflectivity", &planes[i].reflectivity, 0.0f, 1.0f);
                        ImGui::SliderFloat("Specular Falloff", &planes[i].specularFalloff, 10.0f, 100.0f);
                    }
                }
            }
        }

        ImGui::EndMenu();
    }


    static auto lastTime = std::chrono::steady_clock::now();
    static int directionSphere = 1;
    static int directionBox = 1;
    static float sphereT = 0;
    static float boxT = 0;

    auto currentTime = std::chrono::steady_clock::now();
    float delta = std::chrono::duration_cast<std::chrono::duration<float>>(currentTime - lastTime).count();
    sphereT += (delta / 2) * directionSphere;
    boxT += (delta / 5) * directionBox;
    lastTime = currentTime;

    if (sphereT > 1) {
        directionSphere = -1;
    } else if (sphereT < 0) {
        directionSphere = 1;
    }
    if (boxT > 1) {
        directionBox = -1;
    } else if (boxT < 0) {
        directionBox = 1;
    }

    std::vector<vec3> sphereAnimation = std::vector<vec3>{
        vec3{2.0f, 0.0f, 2.0f},
        vec3{2.0f, -3.0f, 2.0f},
        vec3{2.0f, 3.0f, 2.0f},
        vec3{2.0f, 0.0f, 2.0f},
    };

    std::vector<vec3> minimumBoundsAnimation = std::vector<vec3>{
        vec3{0.f, -5.f, -4.f},
        vec3{-4.f, -5.f, -4.f},
        vec3{3.f, -5.f, -4.f},
        vec3{0.f, -5.f, -4.f},
    };
    std::vector<vec3> maximumBoundsAnimation = std::vector<vec3>{
        vec3{0.f, -5.f, -4.f},
        vec3{-3.f, -4.f, -3.f},
        vec3{4.f, -4.f, -3.f},
        vec3{0.f, -5.f, -4.f},
    };

    vec3 interpolatedPosition = interpolate(sphereAnimation, sphereT);
    spheres[0].origin = interpolatedPosition;

    vec3 interpolatedMinimum = interpolate(minimumBoundsAnimation, boxT);
    vec3 interpolateMaximum = interpolate(maximumBoundsAnimation, boxT);
    boxes[0].minimumBounds = interpolatedMinimum;
    boxes[0].maximumBounds = interpolateMaximum;

	shaderProgramRaytrace->setUniform("modelViewProjectionMatrix", modelViewProjectionMatrix);
	shaderProgramRaytrace->setUniform("inverseModelViewProjectionMatrix", inverseModelViewProjectionMatrix);
    shaderProgramRaytrace->setUniform("sphereCount", static_cast<int>(spheres.size()));
    shaderProgramRaytrace->setUniform("boxCount", static_cast<int>(boxes.size()));
    shaderProgramRaytrace->setUniform("planeCount", static_cast<int>(planes.size()));
    shaderProgramRaytrace->setUniform("lightCount", static_cast<int>(lights.size()));
    shaderProgramRaytrace->setUniform("ambientColor", ambientColor);
    shaderProgramRaytrace->setUniform("ambientIntensity", ambientIntensity);
    shaderProgramRaytrace->setUniform("attenConstant", attenConstant);
    shaderProgramRaytrace->setUniform("attenLinear", attenLinear);
    shaderProgramRaytrace->setUniform("attenQuadratic", attenQuadratic);
    shaderProgramRaytrace->setUniform("reflectionCount", reflectionCount);
    shaderProgramRaytrace->setUniform("reflectionDecayRatio", reflectionDecayRatio);

	m_quadArray->bind();

	shaderProgramRaytrace->use();

    // Set the data of the objects in the shader after we start using the shader program 
    int i = 0;
    for (auto s : spheres) {
        std::string index = std::to_string(i);
        glUniform3fv(glGetUniformLocation(shaderProgramRaytrace->id(), ("spheres[" + index + "].origin").c_str()), 1, glm::value_ptr(s.origin));
        glUniform1f(glGetUniformLocation(shaderProgramRaytrace->id(), ("spheres[" + index + "].radius").c_str()), s.radius);
        glUniform3fv(glGetUniformLocation(shaderProgramRaytrace->id(), ("spheres[" + index + "].color").c_str()), 1, glm::value_ptr(s.color));
        glUniform1f(glGetUniformLocation(shaderProgramRaytrace->id(), ("spheres[" + index + "].reflectivity").c_str()), s.reflectivity);
        glUniform1f(glGetUniformLocation(shaderProgramRaytrace->id(), ("spheres[" + index + "].specularFalloff").c_str()), s.specularFalloff);
        i++;
    }
    i = 0;
    for (auto b : boxes) {
        std::string index = std::to_string(i);
        glUniform3fv(glGetUniformLocation(shaderProgramRaytrace->id(), ("boxes[" + index + "].minimumBounds").c_str()), 1, glm::value_ptr(b.minimumBounds));
        glUniform3fv(glGetUniformLocation(shaderProgramRaytrace->id(), ("boxes[" + index + "].maximumBounds").c_str()), 1, glm::value_ptr(b.maximumBounds));
        glUniform3fv(glGetUniformLocation(shaderProgramRaytrace->id(), ("boxes[" + index + "].color").c_str()), 1, glm::value_ptr(b.color));
        glUniform1f(glGetUniformLocation(shaderProgramRaytrace->id(), ("boxes[" + index + "].reflectivity").c_str()), b.reflectivity);
        glUniform1f(glGetUniformLocation(shaderProgramRaytrace->id(), ("boxes[" + index + "].specularFalloff").c_str()), b.specularFalloff);
        i++;
    }
    i = 0;
    for (auto p : planes) {
        std::string index = std::to_string(i);
        glUniform3fv(glGetUniformLocation(shaderProgramRaytrace->id(), ("planes[" + index + "].center").c_str()), 1, glm::value_ptr(p.center));
        glUniform3fv(glGetUniformLocation(shaderProgramRaytrace->id(), ("planes[" + index + "].normal").c_str()), 1, glm::value_ptr(p.normal));
        glUniform3fv(glGetUniformLocation(shaderProgramRaytrace->id(), ("planes[" + index + "].color").c_str()), 1, glm::value_ptr(p.color));
        glUniform1f(glGetUniformLocation(shaderProgramRaytrace->id(), ("planes[" + index + "].reflectivity").c_str()), p.reflectivity);
        glUniform1f(glGetUniformLocation(shaderProgramRaytrace->id(), ("planes[" + index + "].specularFalloff").c_str()), p.specularFalloff);
        i++;
    }
    i = 0;
    for (auto l : lights) {
        std::string index = std::to_string(i);
        glUniform3fv(glGetUniformLocation(shaderProgramRaytrace->id(), ("lights[" + index + "].position").c_str()), 1, glm::value_ptr(l.position));
        glUniform3fv(glGetUniformLocation(shaderProgramRaytrace->id(), ("lights[" + index + "].color").c_str()), 1, glm::value_ptr(l.color));
        glUniform1f(glGetUniformLocation(shaderProgramRaytrace->id(), ("lights[" + index + "].intensity").c_str()), l.intensity);
        i++;
    }
    
	// we are rendering a screen filling quad (as a tringle strip), so we can cast rays for every pixel
	m_quadArray->drawArrays(GL_TRIANGLE_STRIP, 0, 4);
	shaderProgramRaytrace->release();
	m_quadArray->unbind();


	// Restore OpenGL state (disabled to to issues with some Intel drivers)
	// currentState->apply();
}

vec3 RaytraceRenderer::interpolate(std::vector<vec3> ps, float t) {
    vec3 v0 = 2.0f * ps[1];
    vec3 v1 = (-ps[0] + ps[2]) * t;
    vec3 v2 = (2.0f * ps[0] - 5.0f * ps[1] + 4.0f * ps[2] - ps[3]) * static_cast<float>(pow(t, 2));
    vec3 v3 = (-ps[0] + 3.0f * ps[1] - 3.0f * ps[2] + ps[3]) * static_cast<float>(pow(t, 3));
    
    return 0.5f * (v0 + v1 + v2 + v3);
}

